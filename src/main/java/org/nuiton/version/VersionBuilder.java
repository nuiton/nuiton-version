package org.nuiton.version;

/*
 * #%L
 * Nuiton Version
 * %%
 * Copyright (C) 2016 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * To build some {@link Version}.
 *
 * <h3>General usage</h3>
 * Use one the {@code create} methods, custom what you need on build, and finally
 * use {@link #build()} method to obtain a version.
 * <h3>Options details</h3>
 * TODO
 * <ul>
 * <li>{@code preReleaseClassifiers}</li>
 * <li>{@code splitSeparators}</li>
 * <li>{@code joinSeparator}</li>
 * <li>{@code snapshot}</li>
 * </ul>
 * <h3>Customize componants</h3>
 * TODO
 *
 * Created on 7/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class VersionBuilder {

    /**
     * Classifiers known as pre-release classifiers.
     */
    protected Set<String> preReleaseClassifiers;

    /**
     * Set of characters used to split componants.
     */
    protected Set<Character> splitSeparators;

    /**
     * The character to join componant in a string representation.
     */
    protected Character joinSeparator;

    /**
     * The string represention of the version.
     */
    protected String version;

    /**
     * List of componants of the version.
     */
    protected List<Comparable> componants;

    /**
     * List of componants separators explicitly definied (used as {@link Version#componantSeparators}.
     */
    protected List<String> componantSeparators;

    /**
     * Is the version a snapshot ? In a such case, the {@link #version} ends with {@code -SNAPSHOT}.
     */
    protected boolean snapshot;

    public static VersionBuilder create() {
        return new VersionBuilder();
    }

    public static VersionBuilder create(String version) {
        return new VersionBuilder().setVersion(version);
    }

    public static VersionBuilder create(Version version) {

        List<Comparable> componants = toComparableList(version.getComponants());
        List<String> componantSeparators = new ArrayList<String>(version.getComponantSeparators());
        boolean snapshot = version.isSnapshot();
        return new VersionBuilder().setComponants(componants).setComponantSeparators(componantSeparators).setSnapshot(snapshot);

    }

    public static VersionBuilder create(List<Comparable> componants) {
        return new VersionBuilder().setComponants(componants);
    }

    public VersionBuilder() {
        splitSeparators = new HashSet<Character>();
        splitSeparators.add('-');
        splitSeparators.add('.');
        splitSeparators.add('_');
        joinSeparator = '.';
        preReleaseClassifiers = new HashSet<String>();
        preReleaseClassifiers.add("alpha");
        preReleaseClassifiers.add("beta");
        preReleaseClassifiers.add("rc");
    }

    public VersionBuilder setVersion(String version) {
        Preconditions.checkState(!fromComponants(), "Can't set version if componants are filled");
        this.version = version;
        return this;
    }

    public VersionBuilder setComponants(List<Comparable> componants) {
        Preconditions.checkState(!fromStringRepresentation(), "Can't set componants if version is filled");
        this.componants = componants;
        return this;
    }

    public VersionBuilder setComponant(int level, Comparable value) {

        Preconditions.checkState(!fromStringRepresentation(), "Can't set componants if version is filled");
        int size = componants.size();
        Preconditions.checkArgument(level >= 0 && level < size, "level should be in [0, " + (size - 1) + "]");

        componants.set(level, value);
        return this;

    }

    public VersionBuilder addComponant(Comparable value) {

        Preconditions.checkState(!fromStringRepresentation(), "Can't set componants if version is filled");
        componants.add(value);
        return this;

    }

    public VersionBuilder addComponant(Comparable value, char componantSeparator) {

        addComponant(value);

        if (componantSeparators == null) {

            // init it with join separator
            componantSeparators = new ArrayList<String>(componants.size() - 1);

            initSeparatorList(componantSeparators, componants.size() - 2);

        }

        componantSeparators.add(componantSeparator + "");

        return this;

    }

    public VersionBuilder removeComponant(int level) {

        Preconditions.checkState(!fromStringRepresentation(), "Can't set componants if version is filled");
        int size = componants.size();
        Preconditions.checkArgument(level >= 0 && level < size, "level should be in [0, " + (size - 1) + "]");
        this.componants.remove(level);
        if (componantSeparators != null) {

            // remove this separator
            this.componantSeparators.remove(level - 1);

        }
        return this;

    }

    public VersionBuilder setComponantSeparators(List<String> componantSeparators) {

        Preconditions.checkState(!fromStringRepresentation(), "Can't set componants if version is filled");
        Preconditions.checkArgument(componantSeparators.size() == componants.size() - 1, "Must have n-1 componant separators if you have n componants");
        this.componantSeparators = componantSeparators;
        return this;

    }

    public VersionBuilder setComponantSeparator(int level, char value) {

        Preconditions.checkState(!fromStringRepresentation(), "Can't set componants if version is filled");
        int size = componants.size();
        Preconditions.checkArgument(level >= 0 && level < size, "level should be in [0, " + (size - 2) + "]");
        this.componantSeparators.set(level, value + "");
        return this;

    }

    public VersionBuilder setSnapshot(boolean snapshot) {

        Preconditions.checkState(!fromStringRepresentation(), "Can't set snapshot if version is filled");
        this.snapshot = snapshot;
        return this;

    }

    public VersionBuilder setSplitSeparators(Set<Character> splitSeparators) {

        for (Character splitSeparator : splitSeparators) {
            Preconditions.checkState(!CharUtils.isAsciiAlphanumeric(splitSeparator), "Can't use a alphanumeric splitseparator");
        }
        this.splitSeparators = splitSeparators;
        return this;

    }

    public VersionBuilder setJoinSeparator(Character joinSeparator) {
        this.joinSeparator = joinSeparator;
        return this;
    }

    public VersionBuilder setPreReleaseClassifiers(Set<String> preReleaseClassifiers) {
        this.preReleaseClassifiers = preReleaseClassifiers;
        return this;
    }

    protected boolean fromStringRepresentation() {
        return version != null;
    }

    protected boolean fromComponants() {
        return componants != null;
    }

    public Version build() {

        List<Version.VersionComponant> finalComponants = new ArrayList<Version.VersionComponant>();
        List<String> finalComponantSeparators = new ArrayList<String>();

        boolean finalSnapshot;

        if (fromComponants()) {

            finalSnapshot = snapshot;

            for (Comparable componant : componants) {

                boolean number = componant instanceof Integer;
                Version.VersionComponant v = toVersionComponant(number, String.valueOf(componant));
                finalComponants.add(v);

            }

            if (CollectionUtils.isNotEmpty(componantSeparators)) {

                finalComponantSeparators = new ArrayList<String>(componantSeparators);

            } else {

                initSeparatorList(finalComponantSeparators, finalComponants.size() - 1);

            }

        } else if (fromStringRepresentation()) {

            // compute componants + snapshot from version

            finalSnapshot = this.version.endsWith(Version.SNAPSHOT_SUFFIX);

            String versionPart = StringUtils.substringBeforeLast(this.version, Version.SNAPSHOT_SUFFIX);

            // --- split by splitSeparator --- //

            boolean numberState = false;

            String currentComponantStr = null;

            for (int i = 0, l = versionPart.length(); i < l; i++) {

                char currentChar = versionPart.charAt(i);

                if (splitSeparators.contains(currentChar)) {

                    // end of a componant

                    finalComponantSeparators.add(currentChar + "");

                    if (currentComponantStr != null) {

                        // register new componant

                        Version.VersionComponant componant = toVersionComponant(numberState, currentComponantStr);

                        finalComponants.add(componant);

                        currentComponantStr = null;

                    }

                } else {

                    boolean number = CharUtils.isAsciiNumeric(currentChar);

                    if (currentComponantStr == null) {

                        // start a new componant
                        currentComponantStr = "" + currentChar;
                        numberState = number;

                    } else if (numberState == number) {

                        // still on same type
                        // concat to current componant
                        currentComponantStr += currentChar;

                    } else {

                        // new componant detected
                        // finalize the current componant

                        Version.VersionComponant componant = toVersionComponant(numberState, currentComponantStr);
                        finalComponants.add(componant);
                        finalComponantSeparators.add("");

                        // start the new componant

                        numberState = number;
                        currentComponantStr = "" + currentChar;

                    }

                }

            }

            if (currentComponantStr != null) {

                // flush last componant

                Version.VersionComponant componant = toVersionComponant(numberState, currentComponantStr);
                finalComponants.add(componant);

            }

        } else {

            // empty version

            finalSnapshot = snapshot;
            finalComponants.add(new Version.NumberVersionComponant(0));

        }

        if (finalComponantSeparators.isEmpty() && finalComponants.size() > 1) {

            // use componant join separator

            initSeparatorList(finalComponantSeparators, finalComponants.size() - 1);

        }

        Version result = new Version(finalComponants, finalComponantSeparators, finalSnapshot);
        return result;

    }

    protected void initSeparatorList(List<String> list, int size) {

        for (int i = 0; i < size; i++) {

            list.add("" + joinSeparator);

        }

    }

    protected Version.VersionComponant toVersionComponant(boolean numberState, String currentComponantStr) {

        Version.VersionComponant componant;

        if (numberState) {

            // was a number
            componant = new Version.NumberVersionComponant(Integer.valueOf(currentComponantStr));

        } else {

            // was a string
            boolean preRelease = preReleaseClassifiers.contains(currentComponantStr.toLowerCase());

            componant = new Version.StringVersionComponant(preRelease, currentComponantStr);

        }
        return componant;

    }

    protected static List<Comparable> toComparableList(List<Version.VersionComponant> componants) {

        List<Comparable> result = Lists.newArrayList(Lists.transform(componants, new Function<Version.VersionComponant, Comparable>() {
            @Override
            public Comparable apply(Version.VersionComponant input) {
                return input.getValue();
            }
        }));

        return result;

    }

}
