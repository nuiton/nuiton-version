package org.nuiton.version;

/*
 * #%L
 * Nuiton Version
 * %%
 * Copyright (C) 2016 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Comparator of {@link Version}.
 *
 * You may be before this the documentation of {@link Version} object...
 * <h2>Version componant ordering</h2>
 * We can compare only componants of same type.
 *
 * Number componants use natural number ordering (0&lt;1&lt;2,...) (see {@link Version.NumberVersionComponant}).
 *
 * String componants use {@code preRelease} flag order (preRelease is before any other string componant) then natural character ordering of the {@code lowerCase} componant value. (see {@link Version.StringVersionComponant}).
 * <pre>alpha == Alpha, aa &gt; rc</pre>
 * <h2>Snapshot ordering</h2>
 * A {@code snapshot} version is before the exact same version without the flag.
 * <pre>1.1-SNAPSHOT &lt; 1.1</pre>
 * <h2>General ordering algorithm</h2>
 * We compare version componants at same position until there is a difference.
 *
 * If common componants of version are both equals, have a look to the next componant of the longuest version
 * (if the version have the same componants size, Versions are equals!).
 *
 * If the next componant is a number, the longuest version is after the other one:
 * <pre>1 &lt; 1.0</pre>
 * If the next componant is a classifier, we consider the componant {@code preRelease} flag,
 * if setted then the longuest version is before the other one, if no, after:
 * <pre>1-alpha &lt; 1 &lt; 1-aa</pre>
 * <h2>Examples</h2>
 * Here is a list of ordered versions:
 * <pre>
 * 0-SNAPSHOT
 * 0
 * 0.1
 * 0.2
 * 0.2-alpha
 * 0.2-alpha-1
 * 0.2-alpha-2-SNAPSHOT
 * 0.2-alpha-2
 * 0.2-beta
 * 0.2-beta-1
 * 0.2-rc-1
 * 0.2
 * 0.2-aa
 * </pre>
 * Created on 7/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see Version
 * @since 1.0
 */
public class VersionComparator implements Comparator<Version>, Serializable {

    private static final long serialVersionUID = 1L;

    @Override
    public int compare(Version o1, Version o2) {

        int o1NbComponants = o1.getComponantCount();
        int o2NbComponants = o2.getComponantCount();
        int minComponantSize = Math.min(o1NbComponants, o2NbComponants);
        int maxComponantSize = Math.max(o1NbComponants, o2NbComponants);

        int result = 0;

        for (int i = 0; result == 0 && i < minComponantSize; i++) {

            Version.VersionComponant o1Componant = o1.getComponant(i);
            Version.VersionComponant o2Componant = o2.getComponant(i);

            if (Objects.equals(o1Componant.getClass(), o2Componant.getClass())) {

                // same componant type, using natural order
                result = o1Componant.compareTo(o2Componant);

            } else {

                // different componant type
                // classifier is always lower than number componant
                if (o1Componant instanceof Version.NumberVersionComponant) {

                    result = 1;
                } else {

                    result = -1;
                }
            }
        }

        if (result == 0 && minComponantSize != maxComponantSize) {

            // same value base on common componants
            // check type of next componant on the longuest version
            if (o2NbComponants == minComponantSize) {

                // o1 has more componants
                Version.VersionComponant componant = o1.getComponant(minComponantSize);
                if (componant instanceof Version.StringVersionComponant) {

                    // o1 has a string
                    Version.StringVersionComponant stringVersionComponant = (Version.StringVersionComponant) componant;
                    if (stringVersionComponant.isPreRelease()) {

                        // o1 is pre-release so before o2
                        result = -1;

                    } else {

                        // o1 is post release so after o2
                        result = 1;

                    }
                } else {

                    // o1 has one more number componant: after o2
                    result = 1;

                }

            } else {

                // o2 has more componants
                Version.VersionComponant componant = o2.getComponant(minComponantSize);

                if (componant instanceof Version.StringVersionComponant) {

                    // o2 has a string
                    Version.StringVersionComponant stringVersionComponant = (Version.StringVersionComponant) componant;
                    if (stringVersionComponant.isPreRelease()) {

                        // o2 is pre-release so before o1
                        result = 1;

                    } else {

                        // o2 is post release so after o1
                        result = -1;

                    }
                } else {

                    // o2 has one more number componant: after o1
                    result = -1;

                }

            }

        }

        if (result == 0 && ObjectUtils.notEqual(o1.isSnapshot(), o2.isSnapshot())) {

            // snapshot is lower than none snapshot

            if (o2.isSnapshot()) {

                // o2 is snapshot
                result = 1;

            } else {

                // o1 is snapshot
                result = -1;

            }
        }

        return result;

    }

    /**
     * Sort in reverse order the given versions and print them to the standard output.
     *
     * @param args versions to sort
     */
    public static void main(String... args) {

        List<Version> versions = new ArrayList<>();
        for (String arg : args) {
            Version version = Versions.valueOf(arg);
            versions.add(version);
        }
        versions.sort(Collections.reverseOrder(new VersionComparator()));
        String join = Joiner.on(' ').join(versions);
        System.out.println(join);

    }

}
