package org.nuiton.version;

import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.List;

/*
 * #%L
 * Nuiton Version
 * %%
 * Copyright (C) 2016 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Useful class around {@link Version}.
 * <p>
 * Created on 8/23/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class Versions {

    /**
     * Shortcut method to get a version from his string representation.
     *
     * @param version string representation of the version
     * @return converted version from the string representation
     */
    public static Version valueOf(String version) {
        Version v = VersionBuilder.create(version).build();
        return v;
    }

    /**
     * Tests if two versions are equals.
     *
     * @param version0 the first version
     * @param version1 the second version
     * @return {@code true} if versions are equals, {@code false} otherwise.
     */
    public static boolean equals(String version0, String version1) {
        Version v0 = valueOf(version0);
        Version v1 = valueOf(version1);
        boolean result = v0.equals(v1);
        return result;
    }

    /**
     * Tests if the first version is smaller than the second version.
     *
     * @param version0 the first version
     * @param version1 the second version
     * @return {@code true} if {@code version0} is before {@code version1},
     * {@code false} otherwise.
     */
    public static boolean smallerThan(String version0, String version1) {
        Version v0 = valueOf(version0);
        Version v1 = valueOf(version1);
        boolean result = v0.before(v1);
        return result;
    }

    /**
     * Tests if the first version is greater than the second version.
     *
     * @param version0 the first version
     * @param version1 the second version
     * @return {@code true} if {@code version0} is after {@code version1},
     * {@code false} otherwise.
     */
    public static boolean greaterThan(String version0, String version1) {
        Version v0 = valueOf(version0);
        Version v1 = valueOf(version1);
        boolean result = v0.after(v1);
        return result;
    }

    /**
     * Create a version from the given one and set to it the {@code snapshot} state to {@code true}.
     *
     * @param version version to clone
     * @return the cloned version with the {@code snapshot} state to {@code true}
     * @throws IllegalArgumentException if {@code snapshot} state is already set to {@code true} on
     *                                  the given {@code version}.
     */
    public static Version addSnapshot(Version version) {
        if (version.isSnapshot()) {
            throw new IllegalArgumentException(
                    "version " + version + "is already a snapshot");
        }
        Version result = VersionBuilder.create(version).setSnapshot(true).build();
        return result;
    }

    /**
     * Create a version from the given one and set to it the {@code snapshot} state to {@code false}.
     *
     * @param version version to clone
     * @return the cloned version with the {@code snapshot} state to {@code true}
     * @throws IllegalArgumentException if {@code snapshot} state is already set to {@code false} on
     *                                  the given {@code version}
     */
    public static Version removeSnapshot(Version version) {
        if (!version.isSnapshot()) {
            throw new IllegalArgumentException(
                    "version " + version + "is already a snapshot");
        }
        Version result = VersionBuilder.create(version).setSnapshot(false).build();
        return result;
    }

    /**
     * Create a new version containing a single component from a given version.
     *
     * @param version   original version
     * @param component component index to extract
     * @return new {@link Version} with a single component
     */
    public static Version extractVersion(Version version, int component) {
        Version result = extractVersion(version, component, component);
        return result;
    }

    /**
     * Create a new version containing a sub set of component from a given version.
     *
     * @param version        original version
     * @param firstComponent first component index
     * @param lastComponent  last component index
     * @return new {@link Version} with a components sub set
     */
    public static Version extractVersion(Version version, int firstComponent, int lastComponent) {
        if (lastComponent < firstComponent) {
            throw new IllegalArgumentException("lastComponent must be greater or equals to firstComponent");
        }

        // extract components
        List<Comparable> componants = new ArrayList<Comparable>();
        for (int index = firstComponent; index <= lastComponent; index++) {
            Comparable component = version.getComponant(index).getValue();
            componants.add(component);
        }

        Version result = VersionBuilder.create().setComponants(componants).build();
        return result;
    }

    /**
     * Creates a new version from this one incremented.
     * <p>
     * If the last componant is a number, then just increments this number; otherwise add a new
     * number componant with value 1.
     * <p>
     * Example:
     * <ul>
     * <li>1 → 2</li>
     * <li>1-a → 1-a.1</li>
     * </ul>
     *
     * @param version FIXME
     * @return the incremented version
     */
    public static Version increments(Version version) {

        Version result = increments(version, Version.DEFAULT_JOIN_COMPONANT_SEPARATOR);
        return result;

    }

    /**
     * Creates a new version from this one incremented.
     * <p>
     * If the last componant is a number, then just increments this number; otherwise add a new
     * number componant with value 1.
     * <p>
     * Example:
     * <ul>
     * <li>1 → 2</li>
     * <li>1-a → 1-a.1</li>
     * </ul>
     *
     * @param version FIXME
     * @param componantSeperator the componant separator to use the last componant is a classifier
     * @return the incremented version
     */
    public static Version increments(Version version, char componantSeperator) {

        Version newVersion;

        Version.VersionComponant lastComponant = version.getLastComponant();

        if (lastComponant instanceof Version.StringVersionComponant) {

            // must then add new number componant with value 1

            newVersion = VersionBuilder
                    .create(version)
                    .addComponant(1, componantSeperator)
                    .build();

        } else {

            // increments it
            int numberComponant = ((Version.NumberVersionComponant) lastComponant).getValue();

            newVersion = VersionBuilder
                    .create(version)
                    .setComponant(version.getComponantCount() - 1, numberComponant + 1)
                    .build();
        }

        return newVersion;

    }

    /**
     * Creates a new version from this one with the number componant incremented at the given position.
     * <p>
     * <strong>Note:</strong>
     * Will fail if the componant at the required position is not a number.
     *
     * @param version           FIXME
     * @param componantPosition position of the version componant to increment
     * @return the incremented version
     */
    public static Version increments(Version version, int componantPosition) {

        int numberComponant = version.getNumberComponant(componantPosition);

        Version newVersion = VersionBuilder
                .create(version)
                .setComponant(componantPosition, numberComponant + 1)
                .build();

        return newVersion;

    }

    /**
     * Creates a new version from this one with the number componant decremented at the given position.
     * <p>
     * <strong>Note:</strong>
     * Will fail if the componant at the required position is not a number, or his value is 0.
     *
     * @param version FIXME
     * @param componantPosition position of the version componant to increment
     * @return the decremented version
     */
    public static Version decrements(Version version, int componantPosition) {

        int numberComponant = version.getNumberComponant(componantPosition);
        Preconditions.checkArgument(componantPosition > 0, "Componant at position " + componantPosition + " values 0, can't decrement it.");
        Version newVersion = VersionBuilder
                .create(version)
                .setComponant(componantPosition, numberComponant - 1)
                .build();

        return newVersion;

    }
}
