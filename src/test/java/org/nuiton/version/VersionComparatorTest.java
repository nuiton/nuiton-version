package org.nuiton.version;

/*
 * #%L
 * Nuiton Version
 * %%
 * Copyright (C) 2016 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 7/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class VersionComparatorTest {

    @Test
    public void testCompareVersions() {

        VersionComparator comparator = new VersionComparator();

        Version[] versions = new Version[]{
                VersionBuilder.create().setSnapshot(true).build(),
                VersionBuilder.create().build(),
                VersionBuilder.create("1").build(),
                VersionBuilder.create("1.1-alpha-1").build(),
                VersionBuilder.create("1.1-alpha-2").build(),
                VersionBuilder.create("1.1-beta-1").build(),
                VersionBuilder.create("1.1-rc-1-SNAPSHOT").build(),
                VersionBuilder.create("1.1-rc-1").build(),
                VersionBuilder.create("1.1-rc2").build(),
                VersionBuilder.create("1.1").build(),
                VersionBuilder.create("1.1.1").build(),
                VersionBuilder.create("1.1.1-blablah").build(),
                VersionBuilder.create("1.1.1-blablah1-SNAPSHOT").build(),
                VersionBuilder.create("1.1.1-blablah1").build(),
                VersionBuilder.create("1.1.1-blablah1a").build(),
                VersionBuilder.create("1.1.1-blablah1b").build(),
                VersionBuilder.create("2-rc-1").build(),
                VersionBuilder.create("2").build(),
                VersionBuilder.create("2-aa-1").build(),
        };

        for (int i = 0, l = versions.length - 1; i < l; i++) {

            Version v0 = versions[i];
            Version v1 = versions[i + 1];
            Assert.assertTrue(v0 + " < " + v1, comparator.compare(v0, v1) < 0);

        }

    }

}
