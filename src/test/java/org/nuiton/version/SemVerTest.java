package org.nuiton.version;

/*
 * #%L
 * Nuiton Version
 * %%
 * Copyright (C) 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class SemVerTest {

    /**
     * Liste de toutes les versions a tester, de la plus petite a la plus grande
     */
    String[] versions = {
            "0",
            "0.0.0-SNAPSHOT",
            "0.0.0",
            "1.0.0-alpha-SNAPSHOT",
            "1.0.0-alpha",
            "1.0.0-alpha.1",
            "1.0.0-beta.2 ",
            " 1.0.0-beta.11 ",
            " 1.0.0-rc.1 ",
            " 1.0.0-rc.1+build.1-SNAPSHOT ",
            " 1.0.0-rc.1+build.1 ",
            " 1.0.0 ",
            " 1.0.0+0.3.7 ",
            " 1.0.0.23 ",
            " 1.3.7+build ",
            " 1.3.7+build.2.b8f12d7 ",
            " 1.3.7+build.11.e0f985a",
            "99.100-20130127+r123",
    };

    // on doit avoir ici les memes entrees que versions dans le meme ordre
    String[][] versionSplits = {
            {"0", null, null, null},
            {"0.0.0", null, null, "SNAPSHOT"},
            {"0.0.0", null, null, null},
            {"1.0.0", "alpha", null, "SNAPSHOT"},
            {"1.0.0", "alpha", null, null},
            {"1.0.0", "alpha.1", null, null},
            {"1.0.0", "beta.2", null, null},
            {"1.0.0", "beta.11", null, null},
            {"1.0.0", "rc.1", null, null},
            {"1.0.0", "rc.1", "build.1", "SNAPSHOT"},
            {"1.0.0", "rc.1", "build.1", null},
            {"1.0.0", null, null, null},
            {"1.0.0", null, "0.3.7", null},
            {"1.0.0.23", null, null, null},
            {"1.3.7", null, "build", null},
            {"1.3.7", null, "build.2.b8f12d7", null},
            {"1.3.7", null, "build.11.e0f985a", null},
            {"99.100", "20130127", "r123", null},
    };

    @Test
    public void testCreation() {
        for (int i = 0; i < versions.length; i++) {
            SemVer v = new SemVer(versions[i]);
            SemVer vs = SemVer.creator(versionSplits[i]).done();
            Assert.assertEquals(v, vs);

            Assert.assertEquals(versionSplits[i][0], v.getVersion());
            Assert.assertEquals(versionSplits[i][1], v.getPrerelease());
            Assert.assertEquals(versionSplits[i][2], v.getBuild());
            Assert.assertEquals(versionSplits[i][3], v.getSnapshot());
        }
    }

    @Test
    public void testCreator() {
        SemVer v = new SemVer(" 1.0.0-rc.1+build.1-SNAPSHOT ");
        SemVer n = SemVer.creator(v).setVersion(1, 1, 3).setPrerelease("rc.2").setBuild("r123").setSnapshot(false).done();
        Assert.assertEquals("1.1.3-rc.2+r123", n.toString());
    }

    @Test
    public void testInc() {

        String[][] testIncValue = {
                {"0", "1"},
                {"0.0.0-SNAPSHOT", "1.0.0-SNAPSHOT", "0.1.0-SNAPSHOT", "0.0.1-SNAPSHOT"},
                {"0.0.0", "1.0.0", "0.1.0", "0.0.1"},
                {"1.0.0-alpha-SNAPSHOT", "2.0.0-alpha-SNAPSHOT", "1.1.0-alpha-SNAPSHOT", "1.0.1-alpha-SNAPSHOT"},
                {"1.0.0-alpha", "2.0.0-alpha", "1.1.0-alpha", "1.0.1-alpha"},
                {"1.0.0-alpha.1", "2.0.0-alpha.1", "1.1.0-alpha.1", "1.0.1-alpha.1"},
                {" 1.0.0.23 ", " 2.0.0.23 ", " 1.1.0.23 ", " 1.0.1.23 ", " 1.0.0.24 "},
                {"99.100-20130127+r123", "100.100-20130127+r123", "99.101-20130127+r123"},
        };

        for (int i = 1; i < testIncValue.length; i++) {
            SemVer version = new SemVer(testIncValue[i][0]);
            Assert.assertEquals(String.format(
                    "Le nombre de composante doit correspondre au nombre de test a faire %s",
                    Arrays.toString(testIncValue[i])),
                    testIncValue[i].length - 1, version.getVersionCount());
            for (int x = 1; x < testIncValue[i].length; x++) {
                SemVer expected = new SemVer(testIncValue[i][x]);
                SemVer inc = SemVer.creator(version).incVersion(x - 1, 1).done();
                Assert.assertEquals(expected, inc);
            }
        }
    }

    @Test
    public void testIncMajorMinorPatch() {
        // on test seulement sur un exemple, les autres cas sont gere par le testInc
        SemVer version = new SemVer(" 1.0.0.23 ");

        SemVer incMajor = SemVer.creator(version).incMajor().done();
        Assert.assertEquals(new SemVer(" 2.0.0.23 "), incMajor);

        SemVer incMinor = SemVer.creator(version).incMinor().done();
        Assert.assertEquals(new SemVer(" 1.1.0.23 "), incMinor);

        SemVer incPatch = SemVer.creator(version).incPatch().done();
        Assert.assertEquals(new SemVer(" 1.0.1.23 "), incPatch);
    }

    @Test
    public void testGetComposant() {
        SemVer v = new SemVer(" 1.0.3-rc.1+build.1-SNAPSHOT ");
        Assert.assertEquals("1", v.getMajor());
        Assert.assertEquals("0", v.getMinor());
        Assert.assertEquals("3", v.getPatch());

        Assert.assertEquals("rc", v.getPrerelease(0));
        Assert.assertEquals("1", v.getPrerelease(1));

        Assert.assertEquals("build", v.getBuild(0));
        Assert.assertEquals("1", v.getBuild(1));

        Assert.assertEquals("SNAPSHOT", v.getSnapshot());

    }

    @Test
    public void testToJavaIdentifier() {
        SemVer v = new SemVer(" 1.0.3-rc.1+build.1-SNAPSHOT ");
        Assert.assertEquals("1_0_3_rc_1_build_1_SNAPSHOT", v.toJavaIdentifier());
    }

    /**
     * Ce test ne sert pas vraiment en temps normale, mais lorsqu'il y a un
     * test qui fail dans testAll, il est pratique de remettre le test qui
     * echoue plus specifiquement ici pour le debug
     */
    @Test
    public void testOne() {
        SemVer vi = new SemVer(" 1.0.0+0.3.7 ");
        SemVer vj = new SemVer(" 1.0.0.23 ");

        int result = normalize(vi.compareTo(vj));
        int expected = -1;

        Assert.assertTrue(String.format(
                "Bad compare: Compare(%s, %s) = %s, expected %s",
                vi, vj, result, expected),
                expected == result);
//        System.out.println(String.format(
//                "Good compare: Compare(%s, %s) = %s, expected %s",
//                vi, vj, result, expected));
    }

    @Test
    public void testAll() {
        for (int i = 0; i < versions.length; i++) {
            for (int j = 0; j < versions.length; j++) {
                SemVer vi = new SemVer(versions[i]);
                SemVer vj = new SemVer(versions[j]);

                int result = normalize(vi.compareTo(vj));
                int expected = Integer.valueOf(i).compareTo(j); // AThimel 2013/02/26 Rewritten for JDK6 compatibility. Was: Integer.compare(i, j);

                Assert.assertTrue(String.format(
                        "Bad compare: Compare(%s, %s) = %s, expected %s",
                        vi, vj, result, expected),
                        expected == result);
            }
        }
    }

    /**
     * Si le comparator ne renvoi pas -1, 0, 1 on normalize pour avoir -1, 0, 1
     *
     * @param v
     * @return
     */
    protected int normalize(int v) {
        int result = v;
        if (v != 0) {
            result = v / Math.abs(v);
        }
        return result;
    }
}
